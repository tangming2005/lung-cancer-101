As I start the journey with Jay on studing lung cancer, I will put my notes on Lung cancer here.

### histomorphology of four main tyes of lung cancer


Examples of the histomorphology of the four main types of lung cancer:  
[ref](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3163783/)

![](https://gitlab.com/tangming2005/lung-cancer-101/uploads/efdb4c4aecc3318824f2f58df30189d8/Screenshot_2016-10-04_09.52.47.png)

a) squamous cell carcinoma (p63, CK5/6); b) adenocarcinoma (TTF1, CK7); c) large cell carcinoma.  

These three main types constitute the group of non–small cell lung cancers.  

d) Small cell carcinoma (synaptophysin, chromogranin, CD56/NCAM).  

Typical immunochemical marker proteins of each individual entity are listed in parentheses. These may, however, be lacking or expressed in other entities,  
and the immunophenotype should therefore always be interpreted in the morphological context.

>Large cell lung cancers have a reduced expression of the gene E-cadherin,  which may be interpreted as a sign of epithelial-mesenchymal transition. 
The loss of E-cadherin has generally been associated with a poorer survival in patients with non–small cell lung cancers.